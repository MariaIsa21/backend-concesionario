package co.edu.uniajc.backconcesionario.controller;


import co.edu.uniajc.backconcesionario.modelo.Auto;

import co.edu.uniajc.backconcesionario.service.AutosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/autos")
public class AutosController {
  @Autowired
  public AutosService autosService;

  public AutosController(AutosService service){
  }


 public AutosController(){

 }

    @GetMapping
    public ResponseEntity<List<Auto>> ListartodosAutos() {
        return ResponseEntity.ok(autosService.ListarAutos());
    }




}
