package co.edu.uniajc.backconcesionario.controller;
import co.edu.uniajc.backconcesionario.modelo.Cliente;
import co.edu.uniajc.backconcesionario.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/cliente")
public class ClienteController {

    @Autowired
    public ClienteService clienteService;

    public ClienteController(ClienteService service) {
    }

    public ClienteController() {
    }

    @PostMapping
    private ResponseEntity<Cliente> guardar(@RequestBody Cliente cliente) {
        Cliente temp = clienteService.createCliente(cliente);
        try {
            return ResponseEntity.created(new URI("api/cliente" + temp.getTipo_Documento())).body(temp);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @GetMapping
    public ResponseEntity<List<Cliente>> ListarTodosClientes() {
        return ResponseEntity.ok(clienteService.ListarClientes());
    }


    @GetMapping(value = "{id}")
    private ResponseEntity<Optional<Cliente>> ListarClientesPorID(@PathVariable("id") Long id) {
        return ResponseEntity.ok(clienteService.ClienteporID(id));
    }


    @DeleteMapping
    private ResponseEntity<Void> EliminarCliente(@RequestBody Cliente cliente) {
        clienteService.DeleteCliente(cliente);
        return ResponseEntity.ok().build();
    }

    public String analysemood(String message){
        return "sad";
    }

}
