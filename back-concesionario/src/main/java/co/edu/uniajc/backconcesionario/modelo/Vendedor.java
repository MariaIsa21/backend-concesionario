package co.edu.uniajc.backconcesionario.modelo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "vendedor")
public class Vendedor {
        @Id
        private Long Numero_Documento;

        private String Tipo_Documento;
        private String Nombre_Vendedor;
        private Long Celular;
        private String Direccion_Vendedor;
        private String Ciudad;

        public Vendedor(Long Numero_Documento, String Tipo_Documento, String Nombre_Vendedor, Long Celular, String Direccion_Vendedor, String Ciudad) {
            this.Numero_Documento = Numero_Documento;
            this.Tipo_Documento = Tipo_Documento;
            this.Nombre_Vendedor = Nombre_Vendedor;
            this.Celular = Celular;
            this.Direccion_Vendedor = Direccion_Vendedor;
            this.Ciudad = Ciudad;
        }

        public Vendedor(){}

        public Long getNumero_Documento() {
            return Numero_Documento;
        }

        public void setNumero_Documento(Long numero_Documento) {
            Numero_Documento = numero_Documento;
        }

        public String getTipo_Documento() {
            return Tipo_Documento;
        }

        public void setTipo_Documento(String tipo_Documento) {
            Tipo_Documento = tipo_Documento;
        }

        public String getNombre_Vendedor() {
            return Nombre_Vendedor;
        }

        public void setNombre_Vendedor(String nombre_Vendedor) {
            Nombre_Vendedor = nombre_Vendedor;
        }

        public Long getCelular() {
            return Celular;
        }

        public void setCelular(Long celular) {
            Celular = celular;
        }

        public String getDireccion_Vendedor() {
            return Direccion_Vendedor;
        }

        public void setDireccion_Vendedor(String direccion_Vendedor) {
            Direccion_Vendedor = direccion_Vendedor;
        }

        public String getCiudad() {
            return Ciudad;
        }

        public void setCiudad(String ciudad) {
            Ciudad = ciudad;
        }
}
