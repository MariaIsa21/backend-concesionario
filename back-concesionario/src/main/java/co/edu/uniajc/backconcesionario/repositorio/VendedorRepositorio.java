package co.edu.uniajc.backconcesionario.repositorio;

import co.edu.uniajc.backconcesionario.modelo.Vendedor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VendedorRepositorio extends JpaRepository<Vendedor, Long> {
}
