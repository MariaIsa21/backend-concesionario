package co.edu.uniajc.backconcesionario.modelo;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AutoTest {


    private static final Long ID_AUTO = 3L;

    private static final String MARCA = "TOYOTA";

    private static final String LINEA = "FORTUNER 2.5 TURBO LS";

    private static final String TIPO_AUT = "SEDAN";

    private static final String NRO_CHASIS = "C33343HJ";


    Auto pruebaA =  new Auto(ID_AUTO,MARCA,LINEA,TIPO_AUT,NRO_CHASIS);
    private static Long auto1 = 12L;


    @Test
    void getId_Auto() {


        assertEquals(  ID_AUTO , pruebaA.getId_Auto());
        assertNotNull( pruebaA.getId_Auto() );
    }

    @Test
    void setId_Auto() {
        pruebaA.setId_Auto(auto1);
        assertEquals(  auto1, pruebaA.getId_Auto());
    }

    @Test
    void getMarca() {

        assertEquals( MARCA ,  pruebaA.getMarca());
        assertNotNull(pruebaA.getMarca());

    }

    @Test
    void setMarca() {
        pruebaA.setMarca("FIAT");
        assertEquals(  "FIAT" , pruebaA.getMarca());
    }

    @Test
    void getLinea() {
        assertEquals( LINEA ,  pruebaA.getLinea());
        assertNotNull(pruebaA.getLinea());
    }

    @Test
    void setLinea() {
        pruebaA.setLinea("FIAT UNO LS");
        assertEquals(  "FIAT UNO LS", pruebaA.getLinea());
    }

    @Test
    void getTipo_Auto() {
        assertEquals( TIPO_AUT ,  pruebaA.getTipo_Auto());
        assertNotNull(pruebaA.getTipo_Auto());
    }

    @Test
    void setTipo_Auto() {
        pruebaA.setTipo_Auto("OUTSIDER");
        assertEquals(  "OUTSIDER", pruebaA.getTipo_Auto());
    }

    @Test
    void getNro_Chasis() {
        assertEquals( NRO_CHASIS ,  pruebaA.getNro_Chasis());
        assertNotNull(pruebaA.getNro_Chasis());
    }

    @Test
    void setNro_Chasis() {
        pruebaA.setNro_Chasis("333FRT45444");
        assertEquals(  "333FRT45444", pruebaA.getNro_Chasis());

    }
}